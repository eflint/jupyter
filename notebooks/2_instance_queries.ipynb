{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "46f129ae",
   "metadata": {},
   "source": [
    "# Instance queries\n",
    "\n",
    "In the original versions of eFLINT, queries consist of Boolean expressions and their evaluation reports on the success or failure of the query based on whether the expression evaluates to True or False. This notebook describes an extension in which queries can also be formed by writing instances expressions forming so-called *instance queries*. \n",
    "\n",
    "As an example, this notebook describes how instance queries can be used in the context of access control, for example to implement a Policy Administration Point (PAP, see [XACML 3.0](http://docs.oasis-open.org/xacml/3.0/xacml-3.0-core-spec-os-en.html)). In the example we consider read, write and delete actions by actors on assets:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "a585fc25",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": []
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "Fact asset\n",
    "\n",
    "Act read \n",
    "  Actor actor \n",
    "  Related to asset\n",
    "\n",
    "Act write\n",
    "  Actor actor\n",
    "  Related to asset\n",
    "\n",
    "Act delete\n",
    "  Actor actor\n",
    "  Related to asset"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "61369adb",
   "metadata": {},
   "source": [
    "The following fragment defines ownership of actors on assets such that ownership implies the permission to read, write and delete assets."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "ca5bc398",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": []
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "Fact owner-of Identified by actor * asset\n",
    "\n",
    "Extend Act read   Derived from read(owner-of.actor, owner-of.asset) When owner-of\n",
    "Extend Act write  Derived from write(owner-of.actor, owner-of.asset) When owner-of\n",
    "Extend Act delete Derived from delete(owner-of.actor, owner-of.asset) When owner-of"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eb729593",
   "metadata": {},
   "source": [
    "The `owner-of` relation can be populated such that permissions are derived from the elements of the relation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "e486910b",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "+delete(actor(\"Alice\"),asset(\"AliceAccount\"))\n",
       "+owner-of(actor(\"Alice\"),asset(\"AliceAccount\"))\n",
       "+read(actor(\"Alice\"),asset(\"AliceAccount\"))\n",
       "+write(actor(\"Alice\"),asset(\"AliceAccount\"))\n",
       "+delete(actor(\"Bob\"),asset(\"BobAccount\"))\n",
       "+owner-of(actor(\"Bob\"),asset(\"BobAccount\"))\n",
       "+read(actor(\"Bob\"),asset(\"BobAccount\"))\n",
       "+write(actor(\"Bob\"),asset(\"BobAccount\"))\n",
       "+delete(actor(\"Chloe\"),asset(\"ChloeAccount\"))\n",
       "+owner-of(actor(\"Chloe\"),asset(\"ChloeAccount\"))\n",
       "+read(actor(\"Chloe\"),asset(\"ChloeAccount\"))\n",
       "+write(actor(\"Chloe\"),asset(\"ChloeAccount\"))\n"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": []
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "+owner-of(Alice, AliceAccount).\n",
    "+owner-of(Bob,   BobAccount).\n",
    "+owner-of(Chloe, ChloeAccount)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "d39065a4",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Query success\n",
       "Query success\n",
       "Query success\n"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "?Enabled( delete(Alice, AliceAccount)).\n",
    "?Enabled( read  (Bob,   BobAccount)).\n",
    "?!Enabled(read  (Bob,   AliceAccount))."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5ec9c3c3",
   "metadata": {},
   "source": [
    "An access control policy typically consists of a set or sequence of policy rules with each rule stating whether a certain action (e.g., read, write, delete) is permitted or denied. (In case of conflicts between rules, a priority mechanism determines which rule decides on the access decision.)\n",
    "\n",
    "This notion of policy rule is formalised by the following eFLINT fragment."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "f4285310",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": []
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "Fact action       Identified by READ, WRITE, DELETE\n",
    "Fact decision     Identified by PERMIT, DENY\n",
    "Fact policy-rule  Identified by actor * asset * action * decision."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "232fd4bd",
   "metadata": {},
   "source": [
    "Using derivation clauses, policy rules with a PERMIT decision can then be generated based on the permissions established earlier, i.e., based on whether instances of the act-type `read`, `write`, or `delete` are enabled."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "0d36cecd",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "+policy-rule(actor(\"Alice\"),asset(\"AliceAccount\"),action(\"DELETE\"),decision(\"PERMIT\"))\n",
       "+policy-rule(actor(\"Alice\"),asset(\"AliceAccount\"),action(\"READ\"),decision(\"PERMIT\"))\n",
       "+policy-rule(actor(\"Alice\"),asset(\"AliceAccount\"),action(\"WRITE\"),decision(\"PERMIT\"))\n",
       "+policy-rule(actor(\"Bob\"),asset(\"BobAccount\"),action(\"DELETE\"),decision(\"PERMIT\"))\n",
       "+policy-rule(actor(\"Bob\"),asset(\"BobAccount\"),action(\"READ\"),decision(\"PERMIT\"))\n",
       "+policy-rule(actor(\"Bob\"),asset(\"BobAccount\"),action(\"WRITE\"),decision(\"PERMIT\"))\n",
       "+policy-rule(actor(\"Chloe\"),asset(\"ChloeAccount\"),action(\"DELETE\"),decision(\"PERMIT\"))\n",
       "+policy-rule(actor(\"Chloe\"),asset(\"ChloeAccount\"),action(\"READ\"),decision(\"PERMIT\"))\n",
       "+policy-rule(actor(\"Chloe\"),asset(\"ChloeAccount\"),action(\"WRITE\"),decision(\"PERMIT\"))\n"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": []
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "Extend Fact policy-rule \n",
    "  Derived from policy-rule(read.actor,   read.asset,   READ,   PERMIT) When Enabled(read)\n",
    "  Derived from policy-rule(write.actor,  write.asset,  WRITE,  PERMIT) When Enabled(write)\n",
    "  Derived from policy-rule(delete.actor, delete.asset, DELETE, PERMIT) When Enabled(delete)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "4399e8cd",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Query success\n",
       "Query success\n",
       "Query success\n"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "?policy-rule( Alice, AliceAccount, DELETE, PERMIT).\n",
    "?policy-rule( Bob,   BobAccount,   READ,   PERMIT).\n",
    "?!policy-rule(Bob,   AliceAccount, READ,   PERMIT)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "986d558a",
   "metadata": {},
   "source": [
    "The following fragment defines additional derivation rules to generate policy rules for DENY decisions based on absent permissions. However, the fragment does not generate any rules at present due to the fact that `read`, `write` and `delete` are types with infinite domains, implying only instances that hold true will be enumerated (which in this case will also be enabled). This practical design decision makes it possible to apply eFLINT specifications both within runtime (dynamic) systems and to apply eFLINT in contexts where the full domain of discourse is known (a priori). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "5f4e774c",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": []
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "Extend Fact policy-rule\n",
    "  Derived from policy-rule(read.actor,   read.asset,   READ,   DENY)   When !Enabled(read)\n",
    "  Derived from policy-rule(write.actor,  write.asset,  WRITE,  DENY)   When !Enabled(write)\n",
    "  Derived from policy-rule(delete.actor, delete.asset, DELETE, DENY)   When !Enabled(delete)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "88fa4857",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Query success\n"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "?!policy-rule(Bob, AliceAccount, READ, DENY). // cannot yet be generated"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "94f980c4",
   "metadata": {},
   "source": [
    "This problem can be alleviated by closing the domain of discourse which is achieved, in this example, by specifying the known actors and assets:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "3b625d4b",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "+policy-rule(actor(\"Alice\"),asset(\"BobAccount\"),action(\"DELETE\"),decision(\"DENY\"))\n",
       "+policy-rule(actor(\"Alice\"),asset(\"BobAccount\"),action(\"READ\"),decision(\"DENY\"))\n",
       "+policy-rule(actor(\"Alice\"),asset(\"BobAccount\"),action(\"WRITE\"),decision(\"DENY\"))\n",
       "+policy-rule(actor(\"Alice\"),asset(\"ChloeAccount\"),action(\"DELETE\"),decision(\"DENY\"))\n",
       "+policy-rule(actor(\"Alice\"),asset(\"ChloeAccount\"),action(\"READ\"),decision(\"DENY\"))\n",
       "+policy-rule(actor(\"Alice\"),asset(\"ChloeAccount\"),action(\"WRITE\"),decision(\"DENY\"))\n",
       "+policy-rule(actor(\"Bob\"),asset(\"AliceAccount\"),action(\"DELETE\"),decision(\"DENY\"))\n",
       "+policy-rule(actor(\"Bob\"),asset(\"AliceAccount\"),action(\"READ\"),decision(\"DENY\"))\n",
       "+policy-rule(actor(\"Bob\"),asset(\"AliceAccount\"),action(\"WRITE\"),decision(\"DENY\"))\n",
       "+policy-rule(actor(\"Bob\"),asset(\"ChloeAccount\"),action(\"DELETE\"),decision(\"DENY\"))\n",
       "+policy-rule(actor(\"Bob\"),asset(\"ChloeAccount\"),action(\"READ\"),decision(\"DENY\"))\n",
       "+policy-rule(actor(\"Bob\"),asset(\"ChloeAccount\"),action(\"WRITE\"),decision(\"DENY\"))\n",
       "+policy-rule(actor(\"Chloe\"),asset(\"AliceAccount\"),action(\"DELETE\"),decision(\"DENY\"))\n",
       "+policy-rule(actor(\"Chloe\"),asset(\"AliceAccount\"),action(\"READ\"),decision(\"DENY\"))\n",
       "+policy-rule(actor(\"Chloe\"),asset(\"AliceAccount\"),action(\"WRITE\"),decision(\"DENY\"))\n",
       "+policy-rule(actor(\"Chloe\"),asset(\"BobAccount\"),action(\"DELETE\"),decision(\"DENY\"))\n",
       "+policy-rule(actor(\"Chloe\"),asset(\"BobAccount\"),action(\"READ\"),decision(\"DENY\"))\n",
       "+policy-rule(actor(\"Chloe\"),asset(\"BobAccount\"),action(\"WRITE\"),decision(\"DENY\"))\n"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": []
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "Fact actor Identified by Alice, Bob, Chloe.\n",
    "Fact asset Identified by AliceAccount, BobAccount, ChloeAccount."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "9fb810ff",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Query success\n"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "?policy-rule(Bob, AliceAccount, READ, DENY). // can now be generated"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "213f2556",
   "metadata": {},
   "source": [
    "The decision for a particular domain of discourse can also be made specifically (locally) to a single query:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "0ec85edb",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "~policy-rule(actor(\"Alice\"),asset(\"BobAccount\"),action(\"DELETE\"),decision(\"DENY\"))\n",
       "~policy-rule(actor(\"Alice\"),asset(\"BobAccount\"),action(\"READ\"),decision(\"DENY\"))\n",
       "~policy-rule(actor(\"Alice\"),asset(\"BobAccount\"),action(\"WRITE\"),decision(\"DENY\"))\n",
       "~policy-rule(actor(\"Alice\"),asset(\"ChloeAccount\"),action(\"DELETE\"),decision(\"DENY\"))\n",
       "~policy-rule(actor(\"Alice\"),asset(\"ChloeAccount\"),action(\"READ\"),decision(\"DENY\"))\n",
       "~policy-rule(actor(\"Alice\"),asset(\"ChloeAccount\"),action(\"WRITE\"),decision(\"DENY\"))\n",
       "~policy-rule(actor(\"Bob\"),asset(\"AliceAccount\"),action(\"DELETE\"),decision(\"DENY\"))\n",
       "~policy-rule(actor(\"Bob\"),asset(\"AliceAccount\"),action(\"READ\"),decision(\"DENY\"))\n",
       "~policy-rule(actor(\"Bob\"),asset(\"AliceAccount\"),action(\"WRITE\"),decision(\"DENY\"))\n",
       "~policy-rule(actor(\"Bob\"),asset(\"ChloeAccount\"),action(\"DELETE\"),decision(\"DENY\"))\n",
       "~policy-rule(actor(\"Bob\"),asset(\"ChloeAccount\"),action(\"READ\"),decision(\"DENY\"))\n",
       "~policy-rule(actor(\"Bob\"),asset(\"ChloeAccount\"),action(\"WRITE\"),decision(\"DENY\"))\n",
       "~policy-rule(actor(\"Chloe\"),asset(\"AliceAccount\"),action(\"DELETE\"),decision(\"DENY\"))\n",
       "~policy-rule(actor(\"Chloe\"),asset(\"AliceAccount\"),action(\"READ\"),decision(\"DENY\"))\n",
       "~policy-rule(actor(\"Chloe\"),asset(\"AliceAccount\"),action(\"WRITE\"),decision(\"DENY\"))\n",
       "~policy-rule(actor(\"Chloe\"),asset(\"BobAccount\"),action(\"DELETE\"),decision(\"DENY\"))\n",
       "~policy-rule(actor(\"Chloe\"),asset(\"BobAccount\"),action(\"READ\"),decision(\"DENY\"))\n",
       "~policy-rule(actor(\"Chloe\"),asset(\"BobAccount\"),action(\"WRITE\"),decision(\"DENY\"))\n"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "Query success\n",
       "Query success\n"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "Fact actor Identified by Alice, Bob.\n",
    "Fact asset Identified by AliceAccount.\n",
    "\n",
    "?policy-rule(Bob, AliceAccount, READ, DENY).\n",
    "?!policy-rule(Chloe, AliceAccount, READ, DENY). // policy rule not absent because Chloe is not in domain of discourse\n",
    "\n",
    "Fact actor. // resets to original definition\n",
    "Fact asset. // resets to original definition"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "476e1da3",
   "metadata": {},
   "source": [
    "Note that a domain of discourse is established by defining for all types that are either `Identified by Int` or `Identified by String` the values (integers or strings, respectively) that can be used as instances of the type. In what follows we use the following domain of discourse: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "b912e353",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "+policy-rule(actor(\"Alice\"),asset(\"BobAccount\"),action(\"DELETE\"),decision(\"DENY\"))\n",
       "+policy-rule(actor(\"Alice\"),asset(\"BobAccount\"),action(\"READ\"),decision(\"DENY\"))\n",
       "+policy-rule(actor(\"Alice\"),asset(\"BobAccount\"),action(\"WRITE\"),decision(\"DENY\"))\n",
       "+policy-rule(actor(\"Alice\"),asset(\"ChloeAccount\"),action(\"DELETE\"),decision(\"DENY\"))\n",
       "+policy-rule(actor(\"Alice\"),asset(\"ChloeAccount\"),action(\"READ\"),decision(\"DENY\"))\n",
       "+policy-rule(actor(\"Alice\"),asset(\"ChloeAccount\"),action(\"WRITE\"),decision(\"DENY\"))\n",
       "+policy-rule(actor(\"Bob\"),asset(\"AliceAccount\"),action(\"DELETE\"),decision(\"DENY\"))\n",
       "+policy-rule(actor(\"Bob\"),asset(\"AliceAccount\"),action(\"READ\"),decision(\"DENY\"))\n",
       "+policy-rule(actor(\"Bob\"),asset(\"AliceAccount\"),action(\"WRITE\"),decision(\"DENY\"))\n",
       "+policy-rule(actor(\"Bob\"),asset(\"ChloeAccount\"),action(\"DELETE\"),decision(\"DENY\"))\n",
       "+policy-rule(actor(\"Bob\"),asset(\"ChloeAccount\"),action(\"READ\"),decision(\"DENY\"))\n",
       "+policy-rule(actor(\"Bob\"),asset(\"ChloeAccount\"),action(\"WRITE\"),decision(\"DENY\"))\n",
       "+policy-rule(actor(\"Chloe\"),asset(\"AliceAccount\"),action(\"DELETE\"),decision(\"DENY\"))\n",
       "+policy-rule(actor(\"Chloe\"),asset(\"AliceAccount\"),action(\"READ\"),decision(\"DENY\"))\n",
       "+policy-rule(actor(\"Chloe\"),asset(\"AliceAccount\"),action(\"WRITE\"),decision(\"DENY\"))\n",
       "+policy-rule(actor(\"Chloe\"),asset(\"BobAccount\"),action(\"DELETE\"),decision(\"DENY\"))\n",
       "+policy-rule(actor(\"Chloe\"),asset(\"BobAccount\"),action(\"READ\"),decision(\"DENY\"))\n",
       "+policy-rule(actor(\"Chloe\"),asset(\"BobAccount\"),action(\"WRITE\"),decision(\"DENY\"))\n"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": []
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "Fact actor Identified by Alice, Bob, Chloe.\n",
    "Fact asset Identified by AliceAccount, BobAccount, ChloeAccount."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4eda33bb",
   "metadata": {},
   "source": [
    "An *instance query* is of the form `?- <EXPR>.` or `?-- <EXPR>`, with `EXPR` an instance expression evaluationg to zero, one, or more instances. The difference between the latter and the former form is that the latter form only outputs instances that hold true, i.e., it is syntactic sugar for `?- <EXPR> When Holds(<EXPR>)`. \n",
    "\n",
    "The following fragment shows how an instance query (for a certain domain of discourse) can be used to generate all the elements currently in the `policy-rule` relation. Note that the query `?- policy-rule` would generate all the possible elements/instances of the `policy-rule` relation, including the elements that do currently **not** hold true."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "97c5f4b5",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "policy-rule(actor(\"Alice\"),asset(\"ChloeAccount\"),action(\"READ\"),decision(\"DENY\"))\n",
       "policy-rule(actor(\"Alice\"),asset(\"ChloeAccount\"),action(\"WRITE\"),decision(\"DENY\"))\n",
       "policy-rule(actor(\"Alice\"),asset(\"ChloeAccount\"),action(\"DELETE\"),decision(\"DENY\"))\n",
       "policy-rule(actor(\"Alice\"),asset(\"BobAccount\"),action(\"READ\"),decision(\"DENY\"))\n",
       "policy-rule(actor(\"Alice\"),asset(\"BobAccount\"),action(\"WRITE\"),decision(\"DENY\"))\n",
       "policy-rule(actor(\"Alice\"),asset(\"BobAccount\"),action(\"DELETE\"),decision(\"DENY\"))\n",
       "policy-rule(actor(\"Alice\"),asset(\"AliceAccount\"),action(\"READ\"),decision(\"PERMIT\"))\n",
       "policy-rule(actor(\"Alice\"),asset(\"AliceAccount\"),action(\"WRITE\"),decision(\"PERMIT\"))\n",
       "policy-rule(actor(\"Alice\"),asset(\"AliceAccount\"),action(\"DELETE\"),decision(\"PERMIT\"))\n",
       "policy-rule(actor(\"Bob\"),asset(\"ChloeAccount\"),action(\"READ\"),decision(\"DENY\"))\n",
       "policy-rule(actor(\"Bob\"),asset(\"ChloeAccount\"),action(\"WRITE\"),decision(\"DENY\"))\n",
       "policy-rule(actor(\"Bob\"),asset(\"ChloeAccount\"),action(\"DELETE\"),decision(\"DENY\"))\n",
       "policy-rule(actor(\"Bob\"),asset(\"BobAccount\"),action(\"READ\"),decision(\"PERMIT\"))\n",
       "policy-rule(actor(\"Bob\"),asset(\"BobAccount\"),action(\"WRITE\"),decision(\"PERMIT\"))\n",
       "policy-rule(actor(\"Bob\"),asset(\"BobAccount\"),action(\"DELETE\"),decision(\"PERMIT\"))\n",
       "policy-rule(actor(\"Bob\"),asset(\"AliceAccount\"),action(\"READ\"),decision(\"DENY\"))\n",
       "policy-rule(actor(\"Bob\"),asset(\"AliceAccount\"),action(\"WRITE\"),decision(\"DENY\"))\n",
       "policy-rule(actor(\"Bob\"),asset(\"AliceAccount\"),action(\"DELETE\"),decision(\"DENY\"))\n",
       "policy-rule(actor(\"Chloe\"),asset(\"ChloeAccount\"),action(\"READ\"),decision(\"PERMIT\"))\n",
       "policy-rule(actor(\"Chloe\"),asset(\"ChloeAccount\"),action(\"WRITE\"),decision(\"PERMIT\"))\n",
       "policy-rule(actor(\"Chloe\"),asset(\"ChloeAccount\"),action(\"DELETE\"),decision(\"PERMIT\"))\n",
       "policy-rule(actor(\"Chloe\"),asset(\"BobAccount\"),action(\"READ\"),decision(\"DENY\"))\n",
       "policy-rule(actor(\"Chloe\"),asset(\"BobAccount\"),action(\"WRITE\"),decision(\"DENY\"))\n",
       "policy-rule(actor(\"Chloe\"),asset(\"BobAccount\"),action(\"DELETE\"),decision(\"DENY\"))\n",
       "policy-rule(actor(\"Chloe\"),asset(\"AliceAccount\"),action(\"READ\"),decision(\"DENY\"))\n",
       "policy-rule(actor(\"Chloe\"),asset(\"AliceAccount\"),action(\"WRITE\"),decision(\"DENY\"))\n",
       "policy-rule(actor(\"Chloe\"),asset(\"AliceAccount\"),action(\"DELETE\"),decision(\"DENY\"))\n",
       "\n"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "?--policy-rule."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0dd31a0d",
   "metadata": {},
   "source": [
    "The instance expression above is formed by using `policy-rule` as a variable. Using `policy-rule` as a constructor we can refine the kinds of policy rules we are looking for. For example, the following fragment asks for all the policy rules with DENY decisions for actor Alice."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "c2ff0bff",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "policy-rule(actor(\"Alice\"),asset(\"BobAccount\"),action(\"READ\"),decision(\"DENY\"))\n",
       "policy-rule(actor(\"Alice\"),asset(\"ChloeAccount\"),action(\"READ\"),decision(\"DENY\"))\n",
       "policy-rule(actor(\"Alice\"),asset(\"BobAccount\"),action(\"WRITE\"),decision(\"DENY\"))\n",
       "policy-rule(actor(\"Alice\"),asset(\"ChloeAccount\"),action(\"WRITE\"),decision(\"DENY\"))\n",
       "policy-rule(actor(\"Alice\"),asset(\"BobAccount\"),action(\"DELETE\"),decision(\"DENY\"))\n",
       "policy-rule(actor(\"Alice\"),asset(\"ChloeAccount\"),action(\"DELETE\"),decision(\"DENY\"))\n",
       "\n"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "?--policy-rule(Alice, asset, action, DENY)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "97a2c1a3",
   "metadata": {},
   "source": [
    "The `give-read-access` action is introduced to make the example more interesting. This action makes it possible for owners of assets to give read access to other actors."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "id": "d0b6de12",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "+give-read-access(actor(\"Alice\"),actor(\"Alice\"),asset(\"AliceAccount\"))\n",
       "+give-read-access(actor(\"Alice\"),actor(\"Bob\"),asset(\"AliceAccount\"))\n",
       "+give-read-access(actor(\"Alice\"),actor(\"Chloe\"),asset(\"AliceAccount\"))\n",
       "+give-read-access(actor(\"Bob\"),actor(\"Alice\"),asset(\"BobAccount\"))\n",
       "+give-read-access(actor(\"Bob\"),actor(\"Bob\"),asset(\"BobAccount\"))\n",
       "+give-read-access(actor(\"Bob\"),actor(\"Chloe\"),asset(\"BobAccount\"))\n",
       "+give-read-access(actor(\"Chloe\"),actor(\"Alice\"),asset(\"ChloeAccount\"))\n",
       "+give-read-access(actor(\"Chloe\"),actor(\"Bob\"),asset(\"ChloeAccount\"))\n",
       "+give-read-access(actor(\"Chloe\"),actor(\"Chloe\"),asset(\"ChloeAccount\"))\n"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": []
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "Act give-read-access \n",
    "  Actor actor\n",
    "  Related to actor', asset\n",
    "  Holds when owner-of()\n",
    "  Creates read(actor', asset)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7237d13e",
   "metadata": {},
   "source": [
    "An additional policy-rule with a PERMIT decision is generated when Chloe executes the `give-read-access` action to give read access to Alice:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "id": "d883aaa4",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "~policy-rule(actor(\"Alice\"),asset(\"ChloeAccount\"),action(\"READ\"),decision(\"DENY\"))\n",
       "+read(actor(\"Alice\"),asset(\"ChloeAccount\"))\n",
       "+policy-rule(actor(\"Alice\"),asset(\"ChloeAccount\"),action(\"READ\"),decision(\"PERMIT\"))\n"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "Executed transition: give-read-access(actor(\"Chloe\"),actor(\"Alice\"),asset(\"ChloeAccount\"))\n",
       "\n"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "give-read-access(Chloe, Alice, ChloeAccount)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "363613a8",
   "metadata": {},
   "source": [
    "Note that the corresponding rule with the DENY decision was removed from the knowledge base.\n",
    "\n",
    "The following instance query is used to generate all the policy rules for permitted actions on Chloe's accounts."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "id": "9536c61d",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "policy-rule(actor(\"Alice\"),asset(\"ChloeAccount\"),action(\"READ\"),decision(\"PERMIT\"))\n",
       "policy-rule(actor(\"Chloe\"),asset(\"ChloeAccount\"),action(\"READ\"),decision(\"PERMIT\"))\n",
       "policy-rule(actor(\"Chloe\"),asset(\"ChloeAccount\"),action(\"WRITE\"),decision(\"PERMIT\"))\n",
       "policy-rule(actor(\"Chloe\"),asset(\"ChloeAccount\"),action(\"DELETE\"),decision(\"PERMIT\"))\n",
       "\n"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "?--policy-rule(actor, ChloeAccount, action, PERMIT). "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dd4d7f0a",
   "metadata": {},
   "source": [
    "The expressions used in instance queries can be arbitrary (instance) expressions. For example, the following instance query uses the `When` operator and the projection operator `<EXPR>.<FIELD-NAME>` to generate all the actors with READ permissions on Chloe's account. The expression also relies on constructor application with explicit field names (see the notebook [1_tutorial voting](https://gitlab.com/eflint/jupyter/-/blob/master/notebooks/1_tutorial_voting.ipynb#Constructor-application)). Note that it is necessary to use the form `?-<EXPR>` for this query as otherwise the returned actors have to hold true in the knowledge base, resulting in the empty set of instances."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c8e366d0",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "actor(\"Alice\")\n",
       "actor(\"Chloe\")\n",
       "\n"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "?-actor When Holds(policy-rule(decision = PERMIT, action = READ, asset = ChloeAccount))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "eFLINT",
   "language": "eFLINT",
   "name": "eflint_kernel"
  },
  "language_info": {
   "codemirror_mode": "null",
   "file_extension": ".eflint",
   "mimetype": "x/eflint",
   "name": "eFLINT",
   "pygments_lexer": "Text",
   "version": "1.0.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
